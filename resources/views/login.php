<html lang="en-US">
<head>
    <?= include_once BASE_PATH . VIEW_LAYOUT_PATH . 'head.php' ?>
    <title><?= 'Login' ?></title>
    <style>
        .form-label-group {
            padding-bottom: 15px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Sign In</h5>
                    <form class="form-login" action="<?= router()->generate('post_login_route') ?>" method="post">
                        <div class="form-label-group">
                            <input type="email" name="email" id="inputLogin" class="form-control"
                                   placeholder="Put your email" required autofocus>
                        </div>
                        <div class="form-label-group">
                            <input name="password" type="password" id="inputPassword" class="form-control"
                                   placeholder="Password" required>
                        </div>
                        <a class="float-left" href="<?= router()->generate('index_route') ?>" title="Go to Tasks"><i
                                    class="fa fa-tasks"></i></a>
                        <button class="btn btn-primary float-right" type="submit">Sign in</button>
                    </form>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
