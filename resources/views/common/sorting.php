<a title="Sorting by <?= $field ?>"
   href="<?= url_generator()->generate($routeName, ['orderBy' => $field, 'type' => 'desc']) ?>">
    <i class="fa fa-arrow-down"></i>
</a>
<a title="Sorting by <?= $field ?>"
   href="<?= url_generator()->generate($routeName, ['orderBy' => $field, 'type' => 'asc']) ?>">
    <i class="fa fa-arrow-up"></i>
</a>
