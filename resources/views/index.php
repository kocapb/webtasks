<html lang="en-US">
<head>
    <?= include_once BASE_PATH . VIEW_LAYOUT_PATH . 'head.php' ?>
    <title><?= 'Task list' ?></title>
    <style>
        .form-label-group {
            padding-bottom: 15px;
        }

        .table {
            margin-top: 10px;
        }
    </style>
</head>
<body>
<div class="container">
    <div>
        <div class="float-md-left">
            <a href="<?= router()->generate('task_get_create_route') ?>" type="button" title="Create task">
                <i class="fa fa-2x fa-plus"></i>
            </a>
        </div>
        <div class="float-md-right">
            <?php if (!auth()) { ?>
                <a href="<?= router()->generate('get_login_route') ?>" title="Login">
                    <i class="fa fa-2x fa-sign-in"></i>
                </a>
            <?php } else { ?>
                <span>

                <a href="<?= router()->generate('logout_route') ?>" title="LogOut">
                    <i class="fa fa-2x fa-sign-out"></i>
                </a>
            </span>

            <?php } ?>
        </div>
    </div>
    <?php if (!empty($tasks)) {
        echo
            '<table class="table">' .
            '<thead>' .
            '<th scope="col">id</th>' .
            '<th scope="col">name' .
            view_template('common/sorting.php', ['field' => 'name', 'routeName' => 'index_route']) .
            '</th>' .
            '<th scope="col">email' .
            view_template('common/sorting.php', ['field' => 'email', 'routeName' => 'index_route']) .
            '</th>' .
            '<th scope="col">description</th>' .
            '<th scope="col">status' .
            view_template('common/sorting.php', ['field' => 'status', 'routeName' => 'index_route']) .
            '</th>' .
            '<th scope="col"><a href="' . url_generator()->generate('index_route') . '" title="Reset sorting"><i class="fa fa-times"></i></a> </th>' .
            '</thead>' .
            '<tbody>';
        foreach ($tasks as $task) {
            $done = ($task->status) ? 'done' : 'new';
            $adminControl = (auth()) ?
                '<a href="/task/' . $task->id . '"><i class="fa fa-edit"></i></a>' : '';
            echo
                '<tr>' .
                '<th scope="row">' . $task->id . '</th>' .
                '<td>' . $view->escape($task->name) . '</td>' .
                '<td>' . $view->escape($task->email) . '</td>' .
                '<td>' . $view->escape($task->description) . '</td>' .
                '<td>' . $done . '</td>' .
                '<td>' . $adminControl . '</td>' .
                '</tr>';
        }
        echo
            '</tbody>' .
            '</table>';
    }
    ?>
    <?= $tasks->links() ?>
</div>
</body>
</html>
