<html lang="en-US">
<head>
    <?= include_once BASE_PATH . VIEW_LAYOUT_PATH . 'head.php' ?>
    <title><?= 'Task list' ?></title>
    <style>
        .form-label-group {
            padding-bottom: 15px;
        }
    </style>
</head>
<body>
<div class="container">
    <form action="<?= (isset($task) && ($task->id)) ? '/task/' . $task->id : '/task/create' ?>" method="post">
        <div class="form-group">
            <label for="taskName">Task name</label>
            <input type="text" class="form-control" id="taskName" name="name" required="required"
                   placeholder="Input task name" value="<?= isset($task) ? $task->name : '' ?>">
            <div class="invalid-feedback" <?= isset($errors['name']) ? 'style="display:block"' : '' ?>><?= isset($errors['name']) ? $errors['name'] : '' ?></div>
        </div>
        <div class="form-group">
            <label for="taskEmail">Email address</label>
            <input type="email" class="form-control" id="taskEmail" name="email" placeholder="Input task email"
                   required="required" value="<?= isset($task) ? $task->email : '' ?>">
            <div class="invalid-feedback" <?= isset($errors['email']) ? 'style="display:block"' : '' ?>><?= isset($errors['email']) ? $errors['email'] : '' ?></div>
        </div>
        <div class="form-group">
            <label for="taskDescription">Task description</label>
            <textarea type="text" class="form-control" id="taskDescription" name="description" rows="3"
                      placeholder="Input task description"
                      required="required"><?= isset($task) ? $task->description : '' ?></textarea>
        </div>
        <div class="form-check">
            <input type="checkbox" class="form-check-input" name="status"
                   id="taskStatus" <?= (isset($task) && ($task->status)) ? 'checked="checked"' : '' ?> >
            <label class="form-check-label" for="taskStatus">Done</label>
        </div>
        <button class="btn btn-primary" type="submit">Save</button>
        <a href="<?= url_generator()->generate('index_route') ?>" class="btn btn-secondary">Back</a>
    </form>
</div>
</body>
</html>
