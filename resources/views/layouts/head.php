<?php
return '<meta charset="utf-8">' .
    '<meta http-equiv="X-UA-Compatible" content="IE=edge">' .
    '<meta name="viewport" content="width=device-width, initial-scale=1">' .
    '<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">' .
    '<link href="/css/app.css" rel="stylesheet">' .
    '<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">' .
    '<script src="/js/jquery.min.js"></script>' .
    '<script src="/bootstrap/js/bootstrap.min.js"></script>';
