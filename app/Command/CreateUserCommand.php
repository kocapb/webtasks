<?php

namespace App\Command;

use App\Entities\User;
use App\System\Database\Database;
use Doctrine\ORM\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CreateUserCommand
 * @package App\Command
 */
class CreateUserCommand extends Command
{
    /** @var string */
    protected static $defaultName = 'app:create-user';
    /** @var Database */
    protected $db;

    /**
     * CreateUserCommand constructor.
     * @param string|null $name
     * @throws ORMException
     */
    public function __construct(string $name = null)
    {
        $this->db = new Database(config('database'));
        parent::__construct($name);
    }

    /**
     * Configuration
     */
    protected function configure()
    {
        $this
            ->setDescription('Creates a new user.')
            ->setHelp('This command allows you to create a user...')
            ->addArgument('email', InputArgument::REQUIRED, 'The email of the user.')
            ->addArgument('password', InputArgument::REQUIRED, 'The password of the user.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ORMException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'User Creator',
            '----------------------',
            '',
        ]);
        $output->writeln('Email: ' . $input->getArgument('email'));
        $output->writeln('Password: ' . $input->getArgument('password'));

        $user = new User();
        $user->setPassword($input->getArgument('password'));
        $user->email = $input->getArgument('email');

        $entityManager = $this->db->getEntityManager();
        $entityManager->persist($user);
        $entityManager->flush();
        return 0;
    }
}
