<?php

namespace App\Http\Controller;

use App\Entities\Task;
use App\System\App;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TaskController
 * @package App\Http\Controller
 */
class TaskController extends Controller
{
    /**
     * Method for get task create form
     * @return Response
     */
    public function getCreateAction()
    {
        return $this->response('task/create');
    }

    /**
     * Method for create task
     * @return Response
     */
    public function postCreateAction()
    {
        return $this->factory(new Task());
    }

    /**
     * @param $task
     * @return RedirectResponse|Response
     */
    private function factory($task)
    {
        if (is_null($task)) {
            return $this->response('not_found');
        }

        $data = $this->getRequest()->request->all();
        $data['status'] = $data['status'] ?? false;

        /** @var $errors */
        $errors = $task->fill($data)->validate();

        if (empty($errors)) {
            $task->save();
            return $this->redirect(router()->generate('index_route'));
        }
        return $this->response('task/create', [
            'errors' => $errors,
            'task' => $task,
        ]);
    }

    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function getEditAction($id)
    {
        if (!auth()) {
            return $this->redirect(router()->generate('index_route'));
        }

        /** @var EntityRepository $taskRepository */
        $taskRepository = App::repository(Task::class);

        return $this->response('/task/create', [
            'task' => $taskRepository->find($id)
        ]);
    }

    /**
     * @param $id
     * @return RedirectResponse|Response
     */
    public function postEditAction($id)
    {
        /** @var EntityRepository $taskRepository */
        $taskRepository = App::repository(Task::class);
        /** @var Task $task */
        $task = $taskRepository->find($id);
        return $this->factory($task);
    }
}
