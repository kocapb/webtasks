<?php

namespace App\Http\Controller;

use App\System\Interfaces\IController;
use App\System\View\View;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Controller
 * @package App\Http\Controller
 */
abstract class Controller implements IController
{
    /** @var View */
    protected $view;

    protected $validator;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->setValidator(Validation::createValidator());
        $this->setView(new View());
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }

    /**
     * @param mixed $validator
     */
    public function setValidator($validator): void
    {
        $this->validator = $validator;
    }

    /**
     * @param string $path
     * @param array $data
     * @return Response
     */
    public function response(string $path, array $data = []): Response
    {
        return new Response($this->getView()->make($path, $data));
    }

    /**
     * @return View
     */
    public function getView(): View
    {
        return $this->view;
    }

    /**
     * @param View $view
     */
    public function setView(View $view): void
    {
        $this->view = $view;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return manager();
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return app()->getRequest();
    }

    protected function redirect($url)
    {
        return redirect($url);
    }

}
