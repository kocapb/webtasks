<?php

namespace App\Http\Controller;

use App\Entities\Task;
use App\System\Paginator;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class IndexController
 * @package App\Http\Controller
 */
class IndexController extends Controller
{
    /**
     * @param $page
     * @param $orderBy
     * @param $type
     * @return Response
     */
    public function indexAction($page, $orderBy, $type)
    {
        $page = ($page !== 0) ? --$page : $page;
        /** @var QueryBuilder $qb */
        $qb = manager()->createQueryBuilder()
            ->select('t')
            ->from('App\Entities\Task', 't');

        $qb = $qb->orderBy('t.' . $orderBy, $type);
        $qb = $qb->setFirstResult($page * Task::PAGINATOR_LIMIT)
            ->setMaxResults(Task::PAGINATOR_LIMIT);

        $tasks = new Paginator($qb);
        return $this->response('index', [
            'tasks' => $tasks,
            'template' => $this->getView()->getTemplate(),
        ]);
    }
}
