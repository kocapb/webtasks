<?php

namespace App\Http\Controller;

use App\Entities\User;
use App\System\App;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LoginController
 * @package App\Http\Controller
 */
class AuthController extends Controller
{
    /**
     * @return Response
     */
    public function getLoginAction()
    {
        return $this->response('login');
    }

    /**
     * @throws \Exception
     */
    public function postLoginAction()
    {
        /** @var EntityRepository $userRepository */
        $userRepository = App::repository(User::class);
        /** @var User $user */
        $user = $userRepository->findOneBy(['email' => filter_var($this->getRequest()->get('email'), FILTER_VALIDATE_EMAIL)]);
        /** @var string $error */
        $error = 'Not such user';
        if ($user) {
            if (password_verify($this->getRequest()->get('password'), $user->getAuthPassword())) {
                $_SESSION['uid'] = GUID();
                $_SESSION['PHPSESSID'] = $_COOKIE['PHPSESSID'];
                return redirect(router()->generate('index_route'));
            } else {
                $error = 'Password incorrect';
            }
        }

        return $this->response('login', ['error' => $error,]);
    }

    /**
     * @return Response
     */
    public function logoutAction()
    {
        session_destroy();
        return $this->redirect(router()->generate('index_route'));
    }

}