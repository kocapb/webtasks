<?php

namespace App\System;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Router;

/**
 * Class App
 * @package App\System
 */
class App
{
    /** @var null */
    private static $instance = null;
    /** @var string */
    private $basePath;
    /** @var Request */
    private $request;
    /** @var Router */
    private $router;
    /** @var RouteCollection */
    private $routes;
    /** @var RequestContext */
    private $requestContext;
    /** @var array */
    private $arguments;
    /** @var mixed */
    private $controller;
    /** @var array */
    private $container = [];

    /**
     * App constructor.
     * @param string $basePath
     */
    private function __construct(string $basePath = null)
    {
        $this->setBasePath($basePath);
        $this->setRequest(Request::createFromGlobals());
        $this->setRequestContext(new RequestContext());
        $this->setRouter(new Router(
            new YamlFileLoader(new FileLocator([__DIR__])),
            $this->getBasePath() . ROUTERS,
            array('cache_dir' => $this->getBasePath() . CACHE_DIR)
        ));
        $this->setRoutes($this->getRouter()->getRouteCollection());
    }

    /**
     * @return mixed
     */
    public function getBasePath()
    {
        return $this->basePath;
    }

    /**
     * @param string $basePath
     */
    public function setBasePath(string $basePath = null): void
    {
        $this->basePath = $basePath;
    }

    /**
     * @return Router
     */
    public function getRouter(): Router
    {
        return $this->router;
    }

    /**
     * @param Router $router
     */
    public function setRouter(Router $router): void
    {
        $this->router = $router;
    }

    public static function repository(string $entityName)
    {
        return static::getInstance()->getContainer('database')->getEntityManager()->getRepository($entityName);
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function getContainer(string $key)
    {
        if (isset($this->container[$key])) {
            return $this->container[$key];
        }

        return null;
    }

    /**
     * @param string $bashPath
     * @return App
     */
    public static function getInstance(string $bashPath = null): App
    {
        if (is_null(static::$instance)) {
            static::$instance = new static($bashPath);
        }

        return static::$instance;
    }

    /**
     * Start method for class
     */
    public function run()
    {
        $response = null;
        $matcher = new UrlMatcher($this->getRoutes(), $this->getRequestContext());

        try {
            $this->getRequest()->attributes->add($matcher->match($this->getRequest()->getPathInfo()));
            $this->setController();
            $this->setArguments();
            /** @var Response $response */
            $response = call_user_func_array($this->getController(), $this->getArguments());
        } catch (\Exception $exception) {
            //TODO:: logging
            exit($exception->getCode() . ' :: ' . $exception->getMessage());
        }

        $response->send();
    }

    /**
     * @return RouteCollection
     */
    public function getRoutes(): RouteCollection
    {
        return $this->routes;
    }

    /**
     * @param RouteCollection $routes
     */
    public function setRoutes(RouteCollection $routes): void
    {
        $this->routes = $routes;
    }

    /**
     * @return RequestContext
     */
    public function getRequestContext(): RequestContext
    {
        return $this->requestContext;
    }

    /**
     * @param RequestContext $requestContext
     */
    public function setRequestContext(RequestContext $requestContext): void
    {
        $this->requestContext = $requestContext;
        $this->requestContext->fromRequest($this->getRequest());
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }

    /**
     * @return array
     */
    public function getController(): array
    {
        return $this->controller;
    }

    /**
     * Setter for controller
     */
    public function setController(): void
    {
        $this->controller = (new ControllerResolver())->getController($this->getRequest());
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * Setter for arguments
     */
    public function setArguments(): void
    {
        $this->arguments = (new ArgumentResolver())->getArguments($this->getRequest(), $this->getController());
    }

    /**
     * @param string $key
     * @param $object
     */
    public function addContainer(string $key, $object): void
    {
        $this->container[$key] = $object;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }
}
