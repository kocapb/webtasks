<?php


namespace App\System\Database;

use App\System\Interfaces\IDatabase;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;

/**
 * Class Database
 * @package App\System\Database
 */
class Database implements IDatabase
{
    /** @var array */
    private $params = [];

    private $entityManager;

    /**
     * Database constructor.
     * @param array $params
     * @param null $entityManager
     * @throws ORMException
     */
    public function __construct(array $params, $entityManager = null)
    {
        $this->setParams($params);
        $this->setEntityManager(EntityManager::create($this->getParams(),
            Setup::createAnnotationMetadataConfiguration([ENTITY_PATH], DEVELOPMENT)));
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    /**
     * @return mixed|EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param mixed $entityManager
     */
    public function setEntityManager($entityManager): void
    {
        $this->entityManager = $entityManager;
    }


}
