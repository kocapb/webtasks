<?php

namespace App\System\Config;

use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Yaml\Yaml;

/**
 * Class YamlConfigLoader
 * @package App\System\Config
 */
class YamlConfigLoader extends FileLoader
{
    /** @var string */
    CONST EXTENSION = 'yaml';

    /**
     * @param string $resource
     * @param string $type
     * @return array
     */
    public function load($resource, string $type = null): array
    {
        return Yaml::parse(file_get_contents($resource));
    }

    /**
     * @param string $resource
     * @param string $type
     * @return bool
     */
    public function supports($resource, string $type = null): bool
    {
        return is_string($resource) && static::EXTENSION == pathinfo($resource, PATHINFO_EXTENSION);
    }

}
