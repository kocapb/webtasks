<?php

namespace App\System\Config;

use App\System\Interfaces\IConfig;
use Symfony\Component\Config\FileLocator;

/**
 * Class Config
 * @package App\System\Config
 */
class Config implements IConfig
{
    /** @var array */
    private $config = [];
    /** @var YamlConfigLoader */
    private $loader;
    /** @var FileLocator */
    private $locator;

    /**
     * Config constructor.
     * @param string $dir
     */
    public function __construct(string $dir)
    {
        $this->setLocator([BASE_PATH . '/' . $dir]);
        $this->setLoader();
    }

    /**
     * Set list of files
     * @param array $list
     */
    public function addList(array $list): void
    {
        foreach ($list as $file) {
            $this->add($file);
        }
    }

    /**
     * @param string $file
     */
    public function add(string $file): void
    {
        /** @var array $configValues */
        $configValues = $this->getLoader()->load($this->getLocator()->locate($file));
        if ($configValues) {
            foreach ($configValues as $key => $configValue) {
                $this->setConfig($key, $configValue);
            }
        }
    }

    /**
     * @return YamlConfigLoader
     */
    public function getLoader(): YamlConfigLoader
    {
        return $this->loader;
    }

    public function setLoader(): void
    {
        $this->loader = new YamlConfigLoader($this->getLocator());
    }

    /**
     * @return FileLocator
     */
    public function getLocator(): FileLocator
    {
        return $this->locator;
    }

    /**
     * @param array $dir
     */
    public function setLocator(array $dir): void
    {
        $this->locator = new FileLocator($dir);
    }

    /**
     * @param string $keyValue
     * @description dot as level separator
     * @return mixed|void|null
     * @example 'database.password' will return string of parameter, 'database' return array of parameters
     */
    public function get(string $keyValue)
    {
        list($key, $value) = array_pad(explode('.', $keyValue, 2), 2, null);
        /** @var array $config */
        $config = $this->getConfig();
        if ($key && isset($config[$key])) {
            if ($value && isset($config[$key][$value])) {
                return $config[$key][$value];
            }

            return $config[$key];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function setConfig(string $key, $value): void
    {
        $this->config[$key] = $value;
    }
}
