<?php

namespace App\System\Interfaces;

/**
 * Interface IView
 * @package App\System\Interfaces
 */
interface IView
{
    /**
     * @param string $path
     * @param array $data
     * @return string
     */
    public function make(string $path, array $data = []): string;
}