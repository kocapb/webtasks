<?php

namespace App\System\Interfaces;

interface IEntity
{
    /**
     * Getter entity constraints for validator
     *
     * @return array
     */
    public function constraints(): array;

    /**
     * Save data to database
     *
     * @return bool
     */
    public function save(): bool;
}