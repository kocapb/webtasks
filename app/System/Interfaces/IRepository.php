<?php

namespace App\System\Interfaces;

use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;

interface IRepository
{
    /**
     * @return EntityManager
     */
    public function entityManager(): EntityManager;

    /**
     * @return string
     */
    public function entityName(): string;

    /**
     * @return ObjectRepository
     */
    public function getRepository(): ObjectRepository;
}
