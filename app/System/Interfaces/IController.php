<?php


namespace App\System\Interfaces;

use Symfony\Component\HttpFoundation\Response;

/**
 * Interface IController
 * @package App\System
 */
interface IController
{
    /**
     * @param $path
     * @param array $data
     * @return Response
     */
    public function response(string $path, array $data = []): Response;
}