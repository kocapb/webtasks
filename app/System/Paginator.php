<?php

namespace App\System;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator as ToolsPaginator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Router;

/**
 * Class Paginator
 * @package App\System
 */
class Paginator extends ToolsPaginator
{
    /** @var int */
    CONST PAGE_RANGE = 3;
    /** @var string */
    private $url;
    /** @var int */
    private $pages;
    /** @var int */
    private $currentPage;
    /** @var Request */
    private $request;
    /** @var int */
    private $limit;
    /** @var UrlGenerator */
    private $urlGenerator;
    /** @var string */
    private $routeName;

    /**
     * Paginator constructor.
     * @param Query|QueryBuilder $query
     * @param bool $fetchJoinCollection
     */
    public function __construct($query, $fetchJoinCollection = true)
    {
        parent::__construct($query, $fetchJoinCollection);
        $this->setLimit($query->getMaxResults());
        $this->setPages(($this->count() >= $query->getMaxResults())
            ? ($this->count() % $query->getMaxResults()) + $this->count() / $query->getMaxResults()
            : 1);
        $this->setRequest(request());
        $this->setCurrentPage($this->getRequest()->attributes->has('page')
            ? $this->getRequest()->attributes->get('page') : 1);
        /** @var Router $router */
        $router = router();
        $this->setRouteName($this->getRequest()->attributes->get('_route'));
        $this->setUrlGenerator(new UrlGenerator($router->getRouteCollection(), $router->getContext()));
        $this->getRequest()->attributes->remove('_route');
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function links()
    {
        if ($this->getPages()) {
            echo $this->template();
        }
    }

    /**
     * @return int
     */
    public function getPages(): int
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     */
    public function setPages(int $pages): void
    {
        $this->pages = $pages;
    }

    /**
     * @return string
     */
    public function template()
    {
        return '<nav aria-label="Page navigation example">' .
            '<ul class="pagination">' .
            '<li class="page-item">' .
            '<a title="Previous" class="page-link" href="' . $this->getPreviousUri() . '" aria-label="Previous">' .
            '<span aria-hidden="true">&laquo;</span>' .
            '<span class="sr-only">Previous</span>' .
            '</a>' .
            '</li>' .
            $this->getPageUris() .
            '<li class="page-item">' .
            '<a title="Next" class="page-link" href="' . $this->getNextUri() . '" aria-label="Next">' .
            '<span aria-hidden="true">&raquo;</span>' .
            '<span class="sr-only">Next</span>' .
            '</a>' .
            '</li>' .
            '</ul>' .
            '</nav>';

    }

    /**
     * @return string
     */
    protected function getPreviousUri()
    {
        $this->setRequestParam('page', $this->getCurrentPage() > 1 ? $this->getCurrentPage() - 1 : 1);
        return $this->getUri();
    }

    /**
     * @param $key
     * @param $value
     */
    private function setRequestParam($key, $value): void
    {
        $request = $this->getRequest();
        $request->attributes->set($key, $value);
    }

    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * @param $currentPage
     */
    public function setCurrentPage(int $currentPage): void
    {
        $this->currentPage = $currentPage;
    }

    /**
     * @return string
     */
    private function getUri()
    {
        return $this->getUrlGenerator()->generate($this->getRouteName(), $this->getRequest()->attributes->all());
    }

    /**
     * @return UrlGenerator
     */
    public function getUrlGenerator(): UrlGenerator
    {
        return $this->urlGenerator;
    }

    /**
     * @param UrlGenerator $urlGenerator
     */
    public function setUrlGenerator(UrlGenerator $urlGenerator): void
    {
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @return string
     */
    public function getRouteName(): string
    {
        return $this->routeName;
    }

    /**
     * @param string $routeName
     */
    public function setRouteName(string $routeName): void
    {
        $this->routeName = $routeName;
    }

    /**
     * @return string
     */
    protected function getPageUris(): string
    {
        $view = '';
        $i = ($this->getCurrentPage() >= $this->getLimit()) ? $this->getCurrentPage() - 1 : 1;
        $limit = $this->getPages() > $this->getLimit()
            ? (($this->getCurrentPage() < $this->getLimit()) ? self::PAGE_RANGE : $this->getCurrentPage() + 1)
            : $this->getPages();
        while ($i <= $limit) {
            $class = ($this->getCurrentPage() == $i) ? 'active' : '';
            $view .=
                '<li class="page-item ' . $class . '">' .
                '<a title="Page ' . $i . '" class="page-link" href="' . $this->getPageUri($i) . '"/>' . $i .
                '</>';
            $i++;
        }

        return $view;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @param int $page
     * @return string
     */
    protected function getPageUri(int $page)
    {
        $this->setRequestParam('page', $page);
        return $this->getUri();
    }

    /**
     * @return string
     */
    protected function getNextUri()
    {
        $this->setRequestParam('page', ($this->getCurrentPage() < $this->getPages()) ? $this->getCurrentPage() + 1 : $this->getPages());
        return $this->getUri();
    }

}
