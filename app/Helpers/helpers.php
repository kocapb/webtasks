<?php

use App\System\App;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Router;

/**
 * @return App
 */
if (!function_exists('app')) {
    /**
     * @return App
     */
    function app()
    {
        return App::getInstance();
    }
}

if (!function_exists('config')) {

    /**
     * @param string $keyValue
     * @return mixed
     */
    function config(string $keyValue)
    {
        return App::getInstance()->getContainer('config')->get($keyValue);
    }
}

if (!function_exists('auth')) {
    /**
     * @return bool
     */
    function auth()
    {
        return (isset($_SESSION['PHPSESSID']) && isset($_COOKIE['PHPSESSID']) && $_SESSION['uid']);
    }
}

if (!function_exists('redirect')) {
    /**
     * @param string $url
     * @param array $headers
     * @return RedirectResponse
     */
    function redirect(string $url = '/', array $headers = []): RedirectResponse
    {
        return RedirectResponse::create($url, Response::HTTP_FOUND, $headers);
    }
}

if (!function_exists('manager')) {
    function manager(): EntityManager
    {
        return App::getInstance()->getContainer('database')->getEntityManager();
    }
}

if (!function_exists('request')) {
    /**
     * @return Request
     */
    function request(): Request
    {
        return App::getInstance()->getRequest();
    }
}

if (!function_exists('route')) {
    /**
     * @return Router
     */
    function router(): Router
    {
        return App::getInstance()->getRouter();
    }
}

if (!function_exists('view_template')) {
    /**
     * @param string $file
     * @param array $variables
     * @return false|string|null
     */
    function view_template(string $file, array $variables = [])
    {
        $filePath = BASE_PATH . VIEW_PATH . $file;
        $output = NULL;
        if (file_exists($filePath)) {
            extract($variables);
            ob_start();
            include $filePath;
            $output = ob_get_clean();
        }

        return $output;
    }
}

if (!function_exists('url_generator')) {
    /**
     * @return UrlGenerator
     */
    function url_generator(): UrlGenerator
    {
        /** @var Router $router */
        $router = App::getInstance()->getRouter();
        return new UrlGenerator($router->getRouteCollection(), $router->getContext());
    }
}


function GUID()
{
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535),
        mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}
