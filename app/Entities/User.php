<?php

namespace App\Entities;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\Entities
 * @property string $password
 * @property string $email
 * @Entity()
 * @Table(name="users")
 * @ORM\Id
 */
class User extends Entity
{
    /**
     * @id
     * @Column(type="integer")
     * @GeneratedValue
     */
    public $id;

    /**
     * @Column(type="string", name="email", length=32)
     */
    public $email;

    /**
     * @Column(type="string", name="password", length=128)
     */
    private $password;

    /**
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     */
    public function setPassword($password)
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @return array
     */
    public function constraints(): array
    {
        return [
            'email' => new Assert\Email(),
        ];
    }

    protected function autenficate(string $password)
    {

    }

}
