<?php

namespace App\Entities;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Task
 * @package App\Entities
 * @property integer id
 * @property string email
 * @Entity()
 * @Table(name="tasks")
 * @ORM\Id
 */
class Task extends Entity
{
    const PAGINATOR_LIMIT = 3;

    /**
     * @id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string", name="name", length=32)
     */
    protected $name;

    /**
     * @Column(type="string", name="email", length=32)
     */
    protected $email;

    /**
     * @Column(type="string", name="description", length=255)
     */
    protected $description;

    /**
     * @Column(type="boolean", name="status")
     */
    protected $status = false;

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'description', 'name', 'status'
    ];

    protected $cast = [
        'status' => 'boolval'
    ];

    /**
     * @param bool $value
     */
    public function setStatus(bool $value)
    {
        $this->status = $value;
    }

    /**
     * @return array
     */
    public function constraints(): array
    {
        return [
            'email' => [
                new Assert\Email(),
                new Assert\NotBlank(),
                new Assert\NotNull(),
            ],
            'description' => [
                new Assert\NotBlank(),
                new Assert\NotNull(),
            ],
        ];
    }
}
