<?php

namespace App\Entities;

use App\System\Interfaces\IEntity;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Entity
 * @package App\Entities
 */
abstract class Entity implements IEntity
{
    /** @var ValidatorInterface */
    protected $validator;
    /** @var */
    protected $errors;
    /** @var array */
    protected $fillable = [];
    /** @var array */
    protected $cast = [];
    /** @var array */
    protected $hidden = [];

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * @return array
     */
    public function getHidden(): array
    {
        return $this->hidden;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        try {
            manager()->persist($this);
            manager()->flush();
        } catch (\Exception $exception) {
            /** TODO:: Log errors */
            return false;
        }

        return true;
    }

    /**
     * @return array
     */
    public function validate()
    {
        $constraints = $this->constraints();
        if (empty($constraints)) {
            return [];
        }
        /** @var array $errors */
        $errors = [];
        foreach ($this->constraints() as $key => $constraint) {
            /** @var ConstraintViolationInterface[] $error */
            $error = $this->getValidator()->validate($this->$key, $constraint);
            if (count($error)) {
                $errors[$key] = $error[0]->getMessage();
            }
        }

        return $errors;
    }

    /**
     * @return array
     */
    abstract public function constraints(): array;

    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface
    {
        return Validation::createValidator();
    }

    /**
     * @param mixed $validator
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /**
     * Method for automatically filling in fields
     * @param array $data
     * @return $this
     */
    public function fill(array $data)
    {
        if (!empty($this->getFillable())) {
            foreach ($this->getFillable() as $property) {
                if (isset($data[$property])) {
                    if (isset($this->getCast()[$property])) {
                        $this->$property = call_user_func_array($this->getCast()[$property], [$data[$property]]);
                    } else {
                        $this->$property = $data[$property];
                    }

                }
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getFillable(): array
    {
        return $this->fillable;
    }

    /**
     * @return array
     */
    public function getCast(): array
    {
        return $this->cast;
    }
}
