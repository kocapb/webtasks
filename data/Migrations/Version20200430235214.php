<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200430235214 extends AbstractMigration
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'User Profiles table';
    }

    /**
     * @param Schema $schema
     * @throws SchemaException
     */
    public function up(Schema $schema): void
    {
        $table = $schema->createTable('profiles');
        $table->addColumn('id', 'bigint', [
            'autoincrement' => true,
            'unsigned' => true,
        ])->setNotnull(true);
        $table->addColumn('user_id', 'bigint', [
            'unsigned' => true,
        ])->setNotnull(true);
        $table->addColumn('name', 'string', [
            'length' => 32,
            'default' => null,
        ]);
        $table->addColumn('lastname', 'string', [
            'length' => 32,
            'default' => null,
        ]);
        $table->addColumn('balance', 'decimal', [
            'precision' => 13,
            'scale' => 2,
            'default' => 0.00,
        ]);

        $table->setPrimaryKey(['id']);
        $table->addForeignKeyConstraint($schema->getTable('users'), ['user_id'], ['id'], [
            'onDelete' => 'CASCADE',
        ], 'FK_PROFILE_USER_ID');

    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('profiles');
    }
}
