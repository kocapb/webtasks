<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429112452 extends AbstractMigration
{
    /** @var string */
    protected $tableName = 'users';

    /**
     * Description for migration
     *
     * @return string
     */
    public function getDescription(): string
    {
        return 'Base table migrations: users and webtasks';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {

        $table = $schema->createTable($this->tableName);
        $table->addColumn('id', 'bigint', [
            'autoincrement' => true,
            'unsigned' => true,
        ])->setNotnull(true);
        $table->addColumn('email', 'string', [
            'length' => 32,
        ])->setNotnull(true);
        $table->addColumn('password', 'string', [
            'length' => 128,
        ])->setNotnull(true);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['email']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable($this->tableName);
    }
}
