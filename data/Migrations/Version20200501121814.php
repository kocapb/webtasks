<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200501121814 extends AbstractMigration
{
    /** @var string */
    protected $tableName = 'tasks';

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return 'Tasks create table';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $table = $schema->createTable($this->tableName);
        $table->addColumn('id', 'bigint', [
            'autoincrement' => true,
            'unsigned' => true,
        ])->setNotnull(true);
        $table->addColumn('name', 'string', [
            'length' => 32,
        ])->setNotnull(true);
        $table->addColumn('email', 'string', [
            'length' => 128,
        ])->setNotnull(true);
        $table->addColumn('description', 'string', [
            'length' => 255,
            'default' => null,
        ])->setNotnull(false);
        $table->addColumn('status', 'boolean', [
            'default' => false,
        ]);
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['name']);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $schema->dropTable($this->tableName);
    }
}
