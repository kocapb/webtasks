<?php
define('BASE_PATH', dirname(__DIR__));
define('ROUTERS', '/routes/routes.yaml');
define('CACHE_DIR', '/storage/cache');
define('VIEW_PATH', '/resources/views/');
define('CONFIG_PATH', 'config');
define('ENTITY_PATH', '/app/Entities');
define('DEVELOPMENT', true);
define('CSS_PATH', '/public/css/');
define('VIEW_LAYOUT_PATH', '/resources/views/layouts/');
